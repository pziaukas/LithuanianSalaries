import unittest
import lithuanian_salaries


class LithuanianSalariesTest(unittest.TestCase):
    def test_version(self) -> None:
        self.assertEqual(lithuanian_salaries.__version__, '1.3.1')


if __name__ == '__main__':
    unittest.main()
