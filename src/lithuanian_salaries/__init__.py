"""Global configurations and imports for Lithuanian Salaries tool."""
__version__ = '1.3.1'
__minimal_wage__ = 730  # 2022
__average_wage__ = 1730  # Q1 2022

from .tax import Tax
from .payslip import Payslip
from .company import Company
from .optimizer import GeneticOptimizer
from .cli import display_salaries

__all__ = [Tax, Payslip, Company, GeneticOptimizer, display_salaries]
