## Lithuanian Salaries modeling tool (v1.3.1)

The State Social Insurance Fund Board (SoDra) in Lithuania publicly provides the following summary statistics for the salaries in each[^1] locally registered company:
- Mean ($`m`$)
- Standard deviation ($`s`$)
- 1st quartile (25% percentile)
- Median (50% percentile)
- 3rd quartile (75% percentile) 

The purpose of this tool is to model a company payslip (the list of salaries) that fits the known summary statistics.

[^1]: Applicable to companies that have more than 20 employees.

### Usage

#### Command line interface
```
Usage: salaries [OPTIONS]

  Display a representative list of salaries given the summary statistics.

Options:
  -m, --mean FLOAT          An average value of salaries.  [required]
  -s, --std FLOAT           A standard deviation of salaries.  [required]
  -q, --quartiles FLOAT...  25%, 50%, 75% percentiles of salaries.
  -e, --employees INTEGER   A number of employees.  [default: 100]
  --include-taxes           Display gross salary values.  [default: False]
  --best-of INTEGER         Choose from multiple simulations.  [default: 100]
  --help                    Show this message and exit.
```

#### Example
Input:
```
salaries --mean 2400 --std 1000 --quartiles 1500 2500 3000 --employees 20
```

Output:
```
Company summary statistics:
Mean  2400.00
STD   1000.00
Q1    1500.00
Q2    2500.00
Q3    3000.00
A possible list of (net) salaries:
[ 766] █████████████▎
[ 771] █████████████▍
[ 813] ██████████████▏
[ 892] ███████████████▌
[ 926] ████████████████▏
[ 939] ████████████████▎
[1088] ██████████████████▉
[1161] ████████████████████▏
[1212] █████████████████████
[1297] ██████████████████████▌
[1514] ██████████████████████████▎
[1589] ███████████████████████████▋
[1620] ████████████████████████████▏
[1717] █████████████████████████████▊
[1758] ██████████████████████████████▌
[1815] ███████████████████████████████▌
[1991] ██████████████████████████████████▌
[2136] █████████████████████████████████████▏
[2449] ██████████████████████████████████████████▌
[2881] ██████████████████████████████████████████████████
Simulation statistics:
Mean  2400.80 (+1)
STD    999.79 (-0)
```

### Concept

#### Distribution
It is known that the [Pareto distribution](https://en.wikipedia.org/wiki/Pareto_distribution) can be applied to describing the distribution of wealth in a society.

The aforementioned statistical distribution is characterized by the shape parameter $`\alpha`$
```math
\alpha = 1 + \sqrt{1 + \left( \frac{m}{s} \right)^2} ;
```

and the scale parameter $`x_m`$
```math
x_m = m \cdot \left( 1 - \frac{1}{\alpha} \right) .
```

Since the parameters can be derived using mean $`m`$ and standard deviation $`s`$ only, the quartiles are fixed in place and in-between values are drawn from the distribution.

#### Optimization
A single simulation can be relatively far away from the company summary statistics even if the assumptions about the distribution are correct - it is a random draw after all.

In order to make results realistic multiple simulations are drawn and then optimized using a [genetic algorithm](https://en.wikipedia.org/wiki/Genetic_algorithm) without mutations. 
Technically, the optimization process is terminated when a new generation does not yield any improved results.
